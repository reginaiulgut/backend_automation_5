package homework;

import com.github.javafaker.Faker;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.testng.Assert;

public class TGHomework01 {
    public static void main(String[] args) {

        Response response;

        Faker faker = new Faker();

        // Creating the post request

        response = RestAssured
                .given().log().all()
                .header("Content-Type", "application/json")
                .header("Authorization", "Bearer f40ebe87f3a29fa34d74b3e8bf222ab2a682321f3fdee653b77ce7b54b79eebc")
                .body("{\n" +
                        "    \"firstName\": \"" + faker.name().firstName() + "\",\n" +
                        "    \"lastName\": \"" + faker.name().lastName() + "\",\n" +
                        "    \"email\": \"" + faker.internet().emailAddress() + "\",\n" +
                        "    \"dob\": \"1992-12-15\"\n" +
                        "}")
                .when().post("https://tech-global-training.com/students")
                .then().log().all().extract().response();

        // System.out.println(response.asString());

        int postId = response.jsonPath().getInt("id");

        System.out.println("Id is coming from response " + postId);

        // Creating the get request to fetch specific student
        response = RestAssured
                .given().log().all()
                .header("Content-Type", "application/json")
                .header("Authorization", "Bearer f40ebe87f3a29fa34d74b3e8bf222ab2a682321f3fdee653b77ce7b54b79eebc")
                .when().get("https://tech-global-training.com/students/" + postId)
                .then().log().all().extract().response();

        /*
        // Creating the get request to fetch all students
        response = RestAssured
                .given().log().all()
                .header("Content-Type", "application/json")
                .header("Authorization", "Bearer f40ebe87f3a29fa34d74b3e8bf222ab2a682321f3fdee653b77ce7b54b79eebc")
                .when().get("https://tech-global-training.com/students")
                .then().log().all().extract().response();

         */

        // Creating the PUT request to update the existing student
        response = RestAssured
                .given().log().all()
                .header("Content-Type", "application/json")
                .header("Authorization", "Bearer f40ebe87f3a29fa34d74b3e8bf222ab2a682321f3fdee653b77ce7b54b79eebc")
                .body("{\n" +
                        "    \"firstName\": \"" + faker.name().firstName() + "\",\n" +
                        "    \"lastName\": \"" + faker.name().lastName() + "\",\n" +
                        "    \"email\": \"" + faker.internet().emailAddress() + "\",\n" +
                        "    \"dob\": \"1992-12-15\"\n" +
                        "}")
                .when().put("https://tech-global-training.com/students/"+ postId)
                .then().log().all().extract().response();

        // Creating the get request to update the existing student
        response = RestAssured
                .given().log().all()
                .header("Content-Type", "application/json")
                .header("Authorization", "Bearer f40ebe87f3a29fa34d74b3e8bf222ab2a682321f3fdee653b77ce7b54b79eebc")
                .body("{\n" +
                        "    \"firstName\": \"" + faker.name().firstName() + "\",\n" +
                        "    \"lastName\": \"" + faker.name().lastName() + "\",\n" +
                        "    \"email\": \"" + faker.internet().emailAddress() + "\",\n" +
                        "    \"dob\": \"1992-12-15\"\n" +
                        "}")
                .when().patch("https://tech-global-training.com/students/"+ postId)
                .then().log().all().extract().response();

        int patchId = response.jsonPath().getInt("id");
        Assert.assertEquals(postId, patchId, "Expected id "+postId+ " we found "+ patchId);


        // Creating the get request to delete the specific user
        response = RestAssured
                .given().log().all()
                .header("Content-Type", "application/json")
                .header("Authorization", "Bearer f40ebe87f3a29fa34d74b3e8bf222ab2a682321f3fdee653b77ce7b54b79eebc")
                .when().delete("https://tech-global-training.com/students/" + postId)
                .then().log().all().extract().response();

        /*

        // Creating the get request to delete the all students
        response = RestAssured
                .given().log().all()
                .header("Content-Type", "application/json")
                .header("Authorization", "Bearer f40ebe87f3a29fa34d74b3e8bf222ab2a682321f3fdee653b77ce7b54b79eebc")
                .when().delete("https://tech-global-training.com/students/")
                .then().log().all().extract().response();

         */

    }
}
